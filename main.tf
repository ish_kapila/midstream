module "ec2_instance" {
  source = "git::https://gitlab.com/ish_kapila/pantry.git?ref=main"
  instancename = var.instancename
  instance_type = var.instance_type
}